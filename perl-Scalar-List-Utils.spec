%define mod_name Scalar-List-Utils

Name:    perl-%{mod_name}
Epoch:   4
Version: 1.68
Release: 2
Summary: Common Scalar and List utility subroutines 
License: GPL-1.0-or-later OR Artistic-1.0-Perl
URL:     https://metacpan.org/release/Scalar-List-Utils
Source0: https://cpan.metacpan.org/authors/id/P/PE/PEVANS/%{mod_name}-%{version}.tar.gz

BuildRequires: findutils, gcc, make, perl-interpreter, perl-devel, perl-generators
BuildRequires: perl(Config), perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires: perl(File::Spec), perl(strict), perl(warnings)
BuildRequires: perl(Carp), perl(Exporter), perl(XSLoader)
BuildRequires: perl(B::Deparse), perl(base), perl(constant), perl(IO::File)
BuildRequires: perl(IO::Handle), perl(List::Util), perl(Math::BigInt)
BuildRequires: perl(overload), perl(Symbol), perl(Sub::Util), perl(Test::More)
BuildRequires: perl(threads), perl(threads::shared), perl(Tie::Handle)
BuildRequires: perl(Tie::Scalar), perl(Tie::StdScalar), perl(vars)
Requires: perl(Carp) 

%description
This package contains a selection of subroutines that people have
expressed would be nice to have in the perl core, but the usage would not
really be high enough to warrant the use of a keyword, and the size so
small such that being individual extensions would be wasteful.

%package_help

%prep
%autosetup -n %{mod_name}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}" NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc Changes README
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/List*
%{perl_vendorarch}/Scalar*
%{perl_vendorarch}/Sub*

%files help
%{_mandir}/man*/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 4:1.68-2
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Nov 07 2024 shenzhongwei <shenzhongwei@kylinos.cn> - 4:1.68-1
- Update to 1.68
-  On Perl version 5.40 or later, various `Scalar::Util` functions are now simply aliases to core-provided functions in `builtin::`: + blessed(), refaddr(), reftype(), weaken(), unweaken(), isweak()
-  Remember to list `head` and `tail` in `List::Util` SYNOPSIS
-  Various improvements to internal CI infrastructure
-  Update exotic names test to avoid single quote package separator
-  Don't loop forever in uniqnum.t (GH #130)
-  Apostrophe is no longer special in package names in recent Perl versions
-  Fix operator precedence issue in `t/uniqnum.t`
-  zip() and mesh() should not alias their input values into the returned results (RT156183)
-  Exception message from mesh() should name the correct function
-  Added (empty) Scalar::List::Utils module so that a module exists which matches the name of the distribution (GH #135)

* Thu Jul 27 2023 jiangchuangang <jiangchuangang@huawei.com> - 4:1.63-1
- update to 1.63

* Tue Oct 25 2022 jiangchuangang <jiangchuangang@huawei.com> - 4:1.60-2
- add missing Epoch in changelog

* Thu Nov 18 2021 liudabo <liudabo1@huawei.com> - 4:1.60-1
- upgrade version to 1.60

* Mon Jun 28 2021 GuoCe <guoce@kylinos.cn> - 4:1.56-1
- Rebase to upstream version 1.56

* Mon Jul 27 2020 zhanzhimin <zhanzhimin@huawei.com> - 4:1.55-1
- 1823191 - Rebase to upstream version 1.55.

* Fri May 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 4:1.52-3
- Add Epoch for updating

* Tue Feb 25 2020 openEuler Buildteam <buildteam@openeuler.org> - 3:1.52-2
- Revise help package

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 3:1.52-1
- Package init
